extends Control


const stories = [
	# JAN
	[
		{
			"title": "China alerts WHO",
			"text": "Chinese Health officials inform the WHO about a cluster of 41 patients with a mysterious pneumonia. Preliminary lab results showed a new-type coronavirus had caused the viral pneumonia in central China's Wuhan.",
			"image": null
		}
	],
	# FEB
	[
		{
			"title": "Say hello to COVID-19",
			"text": 'The WHO announces that the formal name for the novel coronavirus is COVID-19. "Co" stands for coronavirus, "Vi" is for virus and "D" is for disease.',
			"image": null
		}
	],
	# MAR
	[
		{
			"title": "Alpine Breeding Grounds",
			"text": "Around 1,000 people working in the tourism industry, along with at least two dozen Austrian tourists, are now stuck in the winter sports town Ischgl due to the quarantine that has quickly been imposed.",
			"image": "ischgl_ski.png"
		}
	],
	# APR
	[
		{
			"title": "Goats Take Over Streets of Welsh Town",
			"text": 'Herds of mountain goats overtook the town of Llandudno over the weekend. They are becoming more and more confident with no people," one resident said.',
			"image": "welsh_goats.png"
		}
	],
	# MAY
	[
		{
			"title": "Food delivery sector booms",
			"text": "Demand for food delivery services has increased by about 20 to 30 per cent since a ban on dining-in kicked in on April 7. Besides hungry customers, new food and beverage operators and workers have also been flocking to the now thriving sector.",
			"image": null
		}
	],
	# JUN
	[
		{
			"title": 'Lockdown easing is "on track"',
			"text": '"' + "We're clearly on track for that plan because of the number of cases coming down and the plan does refer to hospitality and some of the other things that are closed that so many people want to see open." + '"' + " the UK's health secretary said while the country is preparing to lift restrictions.",
			"image": null
		}
	],
	# JUL
	[
		{
			"title": "Encouraging results from vaccine trials",
			"text": "The vaccines used an adenoviral vector, and both report the vaccine as achieving an humoral response to the critical SARS-CoV-2 spike at the glycoprotein receptor's main binding domain as well as T-cell responses.",
			"image": "lab_1.png"
		}
	],
	# AUG
	[
		{
			"title": 'New Zealand suspects "some failure at the border"',
			"text": "New Zealand officials and scientists are eying a breach in isolation security as the possible cause of the first cases of community transmission in the country in 102 days. The cluster of new cases caught the nation by surprise.",
			"image": null
		}
	],
	# SEP
	[
		{
			"title": "E.U. Health Commissioner Worried",
			"text": 'The increasing number of Corona infections throughout Europe worries everyone, including the E.U.’s Health Commissioner Stella Kyriakides. She demanded quick reactions to the alarming situation. “We should not become negligent”, she said.',
			"image": null
		}
	],
	# OCT
	[
		{
			"title": 'Wales Imposes "Firebreak Lockdown"',
			"text": "Wales is heading into a 17-day lockdown on Friday evening, as many parts of Europe reimpose safety measures because of rising coronavirus case counts.",
			"image": "wales.png"
		}
	],
	# NOV
	[
		{
			"title": "Trump rallies cause an increase in cases",
			"text": "Cases of coronavirus have spiked in a series of states where Donald Trump has held campaign rallies, according to a new analysis of infection rates.",
			"image": "rally.png"
		}
	],
	# DEC
	[
		{
			"title": "Churches against relaxations at Christmas",
			"text": '"At Christmas, Jesus was also alone with Mary and Joseph" the German Protestant Church noted, pleading to keep social distancing also during the holidays.',
			"image": "church.png"
		}
	]
]


onready var spawn = $Spawn
onready var timer = $Timer
onready var newspaper_scene = load("res://components/newspaper/newspaper_item.tscn")

var story_index = -1
var current_newspaper = null
var current_target_rotation = 0
var past_newspapers = []


func _ready():
	timer.connect("timeout", self, "_set_new_newspaper")
	_set_new_newspaper() # init first paper


func _process(delta):
	if current_newspaper != null:
		if current_newspaper.rect_scale.x < 1:
			current_newspaper.rect_scale.x += 0.025 * (1 + delta)
			current_newspaper.rect_scale.y += 0.025 * (1 + delta)
			current_newspaper.rect_rotation += 10 * (1 + delta)
			return
			
		var current_rotation = current_newspaper.rect_rotation
		if current_rotation < current_target_rotation or current_rotation > current_target_rotation + 20:
			if current_rotation > 360:
				current_newspaper.rect_rotation = current_rotation - 360
			current_newspaper.rect_rotation += 10 * (1 + delta)


func _set_new_newspaper():
	if current_newspaper != null:
		past_newspapers.append(current_newspaper)

	for newspaper in past_newspapers:
		newspaper.modulate.a *= 0.5

	story_index += 1
	if story_index == len(stories[StateManager.current_month]):
		SceneManager.goto_scene("world")
		return
	
	var current_story = stories[StateManager.current_month][story_index]
	current_newspaper = newspaper_scene.instance()
	current_newspaper.set_data(current_story.title, current_story.text, current_story.image)
	spawn.add_child(current_newspaper)

	current_newspaper.rect_scale.x = 0.001
	current_newspaper.rect_scale.y = 0.001
	current_newspaper.rect_rotation = 90 + randi() % 90

	current_target_rotation = randi() % 15
	if randf() > 0.5:
		current_target_rotation = 360 - current_target_rotation

	timer.start()
