extends ColorRect


onready var title = get_node("VBoxContainer/TitleLabel")
onready var text = get_node("VBoxContainer/HBoxContainer/TextLabel")
onready var image = get_node("VBoxContainer/HBoxContainer/Image")

export var title_txt = ""
var text_txt = ""
var image_path = null


func _ready():
	title.text = title_txt
	text.text = text_txt

	if image_path != null:
		var texture = load("res://assets/newspaper_images/" + image_path)
		image.set_texture(texture)
	else:
		var extra_x = image.rect_min_size.x
		image.get_parent().remove_child(image)
		text.rect_min_size.x += extra_x + 25


func set_data(_title_txt, _text_txt, _image_path):
	title_txt = _title_txt
	text_txt = _text_txt
	image_path = _image_path
