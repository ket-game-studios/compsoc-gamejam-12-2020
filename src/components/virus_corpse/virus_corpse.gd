extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var fadeout_time = 2 # seconds
var does_stick = true
var velocity = Vector2(0, 0)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	modulate.a = max(modulate.a - delta / fadeout_time, 0.0)

func _physics_process(delta):
	if not does_stick:
		position += velocity * delta
