extends Position2D

var virus_position = Vector2()
var virus_velocity = Vector2()


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _physics_process(delta):
	var virus_particles = get_tree().get_nodes_in_group("virus")
	var position_accumulated = Vector2(0, 0)
	var velocity_accumulated = Vector2(0, 0)
	var count = 0
	for virus in virus_particles:
		if virus.is_connected_to_swarm:
			position_accumulated += virus.global_position
			velocity_accumulated += virus.velocity
			count += 1
	virus_position = position_accumulated / count
	virus_velocity = velocity_accumulated / count
	
	for virus in virus_particles:
		virus.attrack_center_of_mass(virus_position, virus_velocity, delta)
	
	if (count > 0):
		position = virus_position


func _on_Area2D_area_entered(area):
	get_tree().call_group("virus", "set_flocking_parameters", 
		area.inner_flock_radius, 
		area.outer_flock_radius,
		area.reunite_radius,
		area.flock_effect_strength
	)


func _on_Area2D_area_exited(area):
	
	# default parameters (sorry for hardcoding)
	get_tree().call_group("virus", "set_flocking_parameters", 
		100.0, 
		400.0,
		300.0,
		.7
	)
