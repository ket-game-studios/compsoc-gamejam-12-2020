extends Node2D

var nr_inhaled_particles = 0
export var level_goal = 50

onready var sad_sprite = $sprite_sad
onready var life_bar = $LifeBar


func _ready():
	life_bar.max_value = level_goal
	life_bar.value = level_goal


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var progress = min(1.0, float(nr_inhaled_particles) / level_goal)
	modulate.r = 1 - progress
	modulate.g = 1.0
	modulate.b = 1 - progress


func _on_collision_body_entered(body):
	body.get_inhaled_by_person()

	nr_inhaled_particles += 1
	life_bar.value -= 1

	if sad_sprite:
		sad_sprite.visible = true

	if nr_inhaled_particles == level_goal:
		EventManager.emit_signal("finished")
	
