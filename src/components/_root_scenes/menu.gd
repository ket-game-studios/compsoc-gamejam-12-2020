extends Control


func _ready():
	get_node('StartButton').connect(
		'gui_input', self, '__on_start'
	)
	pass


func __on_start(event):
	if event.is_pressed():
		SceneManager.goto_scene('news')
