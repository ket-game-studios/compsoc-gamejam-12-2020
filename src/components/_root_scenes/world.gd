extends Node2D


const months = [
	# TODO: change back to actual string when level is ready
	"01_jan", 
	null,#"02_feb", 
	null,#"03_mar", 
	"04_apr", 
	null,#"05_may", 
	null,#"06_jun", 
	"07_jul", 
	"08_aug", 
	null,#"09_sep", 
	null,#"10_oct", 
	"11_nov", 
	"12_dec"
]

const monthly_cases = [
	8_000,
	85_000,
	800_000,
	3_000_000,
	5_800_000,
	10_000_000,
	17_000_000,
	25_000_000,
	34_000_000,
	45_000_000,
	63_000_000,
	75_000_000,
	null
]


func _ready():
	if months[StateManager.current_month] == null:
		_on_month_finished()
		return

	var path = "res://components/world/months/%s.tscn" % months[StateManager.current_month]
	var scene = ResourceLoader.load(path)
	var scene_node = scene.instance()
	add_child(scene_node)
	move_child(scene_node, 0)

	EventManager.connect("finished", self, "_on_month_finished")

	get_node("CanvasLayer/BtnContainer/Button").connect("gui_input", self, "_on_restart")


func _input(event):
	if event.is_action_pressed("ui_restart"):
		_on_restart(event)


func _on_month_finished():
	StateManager.current_month += 1

	var cases = monthly_cases[StateManager.current_month]
	if cases != null:
		StateManager.case_history.append(cases)

	SceneManager.goto_scene("news")


func _on_restart(_event):
	SceneManager.goto_scene("news")
