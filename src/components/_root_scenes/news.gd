extends Control


func _ready():
	if StateManager.current_month == StateManager.Months.END:
		_show_scene("res://components/news_end/news_end.tscn")
	else:
		_show_scene("res://components/newspaper/newspaper_screen.tscn")


func _show_scene(path):
	var scene = ResourceLoader.load(path)
	var scene_node = scene.instance()
	add_child(scene_node)
	move_child(scene_node, 0)  # move it behind the global ui
