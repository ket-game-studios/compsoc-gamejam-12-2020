extends Node2D

onready var death_area = $DeathArea

func _physics_process(_delta):
	var colliding_bodies = death_area.get_overlapping_bodies()
	for body in colliding_bodies:
		if body.has_signal("die"):
			body.emit_signal("die", false)

