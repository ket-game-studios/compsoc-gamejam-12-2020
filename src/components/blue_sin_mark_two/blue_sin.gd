extends Node2D

export var nr_particles = 30

var time_since_last_spawn = 0
var time_between = .1

var droplet_scene = load("res://components/blue_sin_mark_two/droplet.tscn")
onready var droplets = $droplets
onready var center = $centeroo
onready var spawn = $spawnpoint

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	spawn_one()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time_since_last_spawn += delta
	
	while (time_since_last_spawn > time_between and droplets.get_child_count() < nr_particles):
		time_since_last_spawn -= time_between
		spawn_one()

func spawn_one():
	var droplet = droplet_scene.instance()
	droplet.position = spawn.position
	droplet.center = center.position
	droplets.add_child(droplet)
