extends Sprite

var rotation_speed: float = 0.0
var velocity = Vector2(0, -150)
var radius = 1200
var distance = 0.0
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var center = Vector2(0, 0)

# Called when the node enters the scene tree for the first time.
func _ready():
	rotation_speed = rand_range(-1, 1) * PI
	var direction = rand_range(-1, 1) * PI / 4
	velocity = velocity.rotated(direction)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotation += delta * rotation_speed


func _physics_process(delta):
	position += velocity * delta
	distance += velocity.length() * delta
	if distance >= radius:
		queue_free()



func _on_Area2D_body_entered(body):
	body._on_die(false)
	queue_free()
