extends PathFollow2D


export var offset_step = 100


func _process(delta):
	offset += offset_step * delta
