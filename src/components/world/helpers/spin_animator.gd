extends Node2D


export var rotate_step = 180


func _process(delta):
	rotation_degrees += rotate_step * delta
