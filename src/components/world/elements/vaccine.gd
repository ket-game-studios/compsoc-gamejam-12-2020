extends Node2D


var floating_speed
var intial_speed
var dampening_speed
var min_speed
var target_delay

var start_target_position = null
var current_target_node = null
var speed = 0


onready var deathColider = $DeathArea


func _ready():
	get_tree().create_timer(target_delay).connect("timeout", self, "_find_target")  # float around a bit at first


func _process(delta):
	if current_target_node != null:
		var direction = global_position.direction_to(current_target_node.global_position)
		direction *= speed * (1 + delta)
		position += direction

		if speed > min_speed:
			speed -= dampening_speed / 60

		rotation = global_position.angle_to_point(current_target_node.global_position)

	elif start_target_position != null:
		if global_position.distance_to(start_target_position) < 5:
			return
			
		var direction = global_position.direction_to(start_target_position)
		direction *= floating_speed * (1 + delta)
		position += direction

		rotation = global_position.angle_to_point(start_target_position)


func _physics_process(_delta):
	var colliding_bodies = deathColider.get_overlapping_bodies()
	for body in colliding_bodies:
		if body.has_signal("die"):
			body.emit_signal("die", false)
			queue_free()
			return


func _find_target(_x = null):
	var viruses = get_tree().get_nodes_in_group("virus")
	if viruses.size() == 0:
		return

	current_target_node = viruses[randi() % viruses.size()]
	# gotta re-target if it dies otherwise
	current_target_node.connect("die", self, "_find_target", [], 8)

	start_target_position = null  # avoid return to home

	speed = intial_speed
