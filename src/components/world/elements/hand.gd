extends StaticBody2D


const middle_y = 530
const middle_thresold = 100


onready var death_colider = $DeathArea

var death_colider_attached = false


func _ready():
	remove_child(death_colider)


func _process(_delta):
	if global_position.y > middle_y - middle_thresold && global_position.y < middle_y + middle_thresold:
		if !death_colider_attached:
			add_child(death_colider)
			death_colider_attached = true
	else:
		if death_colider_attached:
			remove_child(death_colider)
			death_colider_attached = false


func _physics_process(_delta):
	var colliding_bodies = death_colider.get_overlapping_bodies()
	for body in colliding_bodies:
		if body.has_signal("die"):
			body.emit_signal("die", false)
