extends Node2D

onready var death_area = $DeathArea

export var chance_per_second = 0.01

var targets = {}

func _physics_process(delta):
	for body in targets.keys():
		targets[body] += chance_per_second * delta
		var death = randf()
		
		if body.has_signal("die") && death < targets[body]:
			body.emit_signal("die")

func _on_DeathArea_body_entered(body):
	targets[body] = chance_per_second

func _on_DeathArea_body_exited(body):
	targets.erase(body)
