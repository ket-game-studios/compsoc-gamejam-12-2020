extends Node2D


export var spawn_region_radius = 250
export var spawn_interval = 1.0
export var spawn_count = 100
export var v_floating_speed = 2
export var v_target_delay = .7
export var v_intial_speed = 10
export var v_dampening_speed = 1
export var v_min_speed = 5


var time_since_last_spawn = 0.0


onready var vaccine_scene = load("res://components/world/elements/vaccine.tscn")
onready var spawn_node = $Spawn


func _ready():
	pass


func _process(delta):
	if spawn_count == 0:
		return

	if time_since_last_spawn < spawn_interval:
		time_since_last_spawn += delta
		return

	spawn_count -= 1
	time_since_last_spawn = 0

	var vaccine_node = vaccine_scene.instance()

	vaccine_node.floating_speed = v_floating_speed
	vaccine_node.target_delay = v_target_delay
	vaccine_node.intial_speed = v_intial_speed
	vaccine_node.dampening_speed = v_dampening_speed
	vaccine_node.min_speed = v_min_speed

	var start_target = Vector2(
		spawn_node.global_position.x + (randi() % spawn_region_radius) - (spawn_region_radius / 2),
		spawn_node.global_position.y + (randi() % spawn_region_radius) - (spawn_region_radius / 2)
	)
	vaccine_node.start_target_position = start_target
	
	spawn_node.add_child(vaccine_node)
