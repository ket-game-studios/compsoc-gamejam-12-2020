extends Node2D


export var wait_time = 3.0

onready var spray_timer = $SprayIntervalTimer
onready var particelS = $Particles2D
onready var deathColider = $DeathArea


func _ready():
	spray_timer.connect("timeout", self, "_on_spray_timer")

	spray_timer.wait_time = wait_time
	remove_child(deathColider)


func _on_spray_timer():
	particelS.emitting = true
	particelS.restart()

	yield(get_tree().create_timer(0.1), "timeout")

	add_child(deathColider)

	var actual_cycle_time = particelS.get_lifetime() / particelS.get_speed_scale() * 1.1
	yield(get_tree().create_timer(actual_cycle_time), "timeout")

	remove_child(deathColider)
	spray_timer.start()


func _physics_process(_delta):
	var colliding_bodies = deathColider.get_overlapping_bodies()
	for body in colliding_bodies:
		if body.has_signal("die"):
			body.emit_signal("die", false)
