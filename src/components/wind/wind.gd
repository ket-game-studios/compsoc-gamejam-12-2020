extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var time = 0
var speed1 = 300
var speed2 = 600
var force = Vector2(200, 0)
onready var sprite1 = $Sprite
onready var sprite2 = $Sprite2

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	time += delta
	sprite1.region_rect.position.x = -time * speed1
	sprite2.region_rect.position.x = -time * speed2

func _physics_process(delta):
	var particles = get_overlapping_bodies()
	var trans = global_transform
	for particle in particles:
		var force_global = trans * force - trans * Vector2(0, 0)
		particle.apply_force(force_global, delta)

