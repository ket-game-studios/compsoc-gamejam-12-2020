extends Node2D

var turntable_speed = 5.0
var suction_strength = 50000.0

onready var turntable = $turntable
onready var suction_area = $suction_area
onready var attraction_target = $attraction_target

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):

	# turntable turning
	turntable.rotation += turntable_speed * delta

func _physics_process(delta):
	var particles = suction_area.get_overlapping_bodies()
	for particle in particles:
		var direction: Vector2 = attraction_target.global_position - particle.global_position
		var force = direction.normalized() * suction_strength / direction.length()
		particle.apply_force(force, delta)


func _on_killing_area_body_entered(body):
	body._on_die(true, true)
