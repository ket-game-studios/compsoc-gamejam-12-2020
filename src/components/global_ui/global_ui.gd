extends Control


const month_names = [
	"January", "February", "March", "April", "May", "June",
	"July", "August", "September", "October", "November", "December",
]


onready var month_label = $MonthLabel
onready var chart_node = $"ColorRect/chart"
onready var default_label = $"ColorRect/DefaultLabel"
onready var no_cases_label = $"ColorRect/NoCasesLabel"

func _ready():
	var month = StateManager.current_month
	if month < StateManager.Months.END:
		month_label.text = month_names[month]
	else:
		remove_child(month_label)

	if len(StateManager.case_history) == 0:
		default_label.visible = false
		chart_node.visible = false
		no_cases_label.visible = true

	chart_node.initialize(
		chart_node.LABELS_TO_SHOW.NO_LABEL,
		{
			cases = Color(1.0, 0.3, 0.3),
		}
	)
	# give the chart a nice starting point:
	chart_node.create_new_point({
		label = "",
		values = {
			cases = 0
		}
	})

	for i in range(len(StateManager.case_history)):
		chart_node.create_new_point({
			label = month_names[i],
			values = {
				cases = StateManager.case_history[i]
			}
		})
