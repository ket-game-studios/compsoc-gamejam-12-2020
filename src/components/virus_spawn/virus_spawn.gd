extends Position2D

export var nr_particles_to_spawn = 100
export var particles_per_second = 80
export var initial_velocity = Vector2(300, 0)

export var jitter_position = 10
export var jitter_direction = 2.0 * PI * 0.2
export var target_speed = 100.0

var time_passed = 0
var particleScene = load("res://components/virus_particle/VirusParticle.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time_passed += delta
	var particle_interval = 1.0 / particles_per_second
	while (time_passed > particle_interval && nr_particles_to_spawn > 0):

		# make new particle
		nr_particles_to_spawn -= 1
		time_passed -= particle_interval
		var particle = particleScene.instance()
		particle.target_speed = target_speed
		
		# position
		var x = rand_range(-jitter_position, +jitter_position)
		var y = rand_range(-jitter_position, +jitter_position)
		particle.position = Vector2(x, y)
		
		# movement direction
		var dir = initial_velocity.angle()
		dir += rand_range(-jitter_direction, +jitter_direction)
		particle.velocity = Vector2(cos(dir), sin(dir)) * initial_velocity.length()
		
		# add to scene
		add_child(particle)
