extends Node2D

var is_dragging = false
var drag_start = Vector2(0, 0)
var drag_to = Vector2(0, 0)
var drag_start_world = Vector2(0, 0)
var drag_to_world = Vector2(0, 0)

var max_distance = 130

onready var start_thing = $MouseStart
onready var end_thing = $MousePointer
onready var radius_thing = $ActionRadius

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func world_to_screen():
	drag_start_world = get_canvas_transform().inverse() * drag_start
	drag_to_world = get_canvas_transform().inverse() * drag_to


func position_things():
	start_thing.visible = is_dragging
	end_thing.visible = is_dragging
	radius_thing.visible = is_dragging
	
	# only do the positioning, if it actually be visible (performance)
	if is_dragging:
		world_to_screen()
		
		# positioning
		start_thing.position = drag_start_world
		radius_thing.position = drag_start_world
		end_thing.position = drag_to_world

		# rotate the arrow
		var drag_vector = drag_to_world - drag_start_world
		var angle = drag_vector.angle()
		end_thing.rotation = angle
		
		# scale
		var new_scale = Vector2(1.0, 1.0) * actual_size() / 500
		radius_thing.scale = Vector2(1.0, 1.0) * action_area_radius()
		start_thing.scale = new_scale
		end_thing.scale = new_scale

func actual_size():
	var distance = (drag_to_world - drag_start_world).length()
	return (1 - pow(1.01, -distance)) * max_distance

func action_area_radius():
	return actual_size() * 2

func start_drag(position: Vector2):
	is_dragging = true
	drag_start = position
	drag_to = position

func end_drag(position: Vector2):
	# update ui
	is_dragging = false
	drag_to = position
	world_to_screen()

	get_tree().call_group("virus", 
		"force_input", 
		drag_start_world, 
		(drag_to_world - drag_start_world).normalized(), 
		action_area_radius()
	)


func mouse_position(position: Vector2):
	drag_to = position


func _input(event):
	if event is InputEventMouseButton:
		if (event.button_index == 1 && event.pressed == true):
			start_drag(event.position)
		elif (event.button_index == 1 && event.pressed == false):
			end_drag(event.position)
	elif event is InputEventMouseMotion:
		mouse_position(event.position)

func _physics_process(delta):
	position_things()
