extends KinematicBody2D


# Declare member variables here. Examples:
var target_speed = 100.0
var velocity = Vector2(300, 0)
var acceleration = 70
var direction_randomness = PI * 0.1
var is_connected_to_swarm = true

# for the popping up anim when they appear
var create_anim_progress = 0.0
var create_anum_speed = 2.0

# flocking parameter
var inner_flock_radius = 100.0
var outer_flock_radius = 400.0
var reunite_radius = 300.0
var flock_effect_strength = .7


var rotation_speed = 0
var rotation_damping = 1

onready var sprite = $Sprite
onready var initialScale = sprite.scale
onready var collider = $CollisionShape2D

var corpse_scene = load("res://components/virus_corpse/virus_corpse.tscn")

var consumed = false

signal die()


onready var trail = $Trail

func _ready():
	connect("die", self, "_on_die")

	# scale it up when it spawns: initial scale to 0
	sprite.scale = Vector2(0, 0)

func _process(delta):
	
	# scale it up when it spawns
	if create_anim_progress < 1.0:
		create_anim_progress = min(1.0, create_anim_progress + delta * create_anum_speed)
		var size =  1 - pow(create_anim_progress - 1.0, 2.0)
		sprite.scale = initialScale * size

func _physics_process(delta):
	
	# accelerate / decelerate towards target speed
	var speed = velocity.length()
	if (speed < target_speed):
		speed = min(target_speed, speed + acceleration * delta)
	elif (speed > target_speed):
		speed = max(target_speed, speed - acceleration * delta)
	velocity = velocity.normalized() * speed
	
	# rotation
	if (rotation_speed > 0):
		rotation_speed = max(rotation_speed - delta * rotation_damping, 0)
	else:
		rotation_speed = min(rotation_speed + delta * rotation_damping, 0)
	sprite.rotate(rotation_speed * delta)
	
	# move particle
	velocity = move_and_slide(velocity)
	
	# set trail
	trail.velocity = velocity
	
func force_input(center: Vector2, direction: Vector2, input_range: float):
	
	var distance = (global_position - center).length()
	var is_affected = distance < input_range
	if is_affected:
		var direction_jitter_rot = rand_range(-direction_randomness, direction_randomness)
		direction = direction.rotated(direction_jitter_rot)
		var effect = 1 - (distance / input_range)
		var new_velocity = direction * input_range
		var old_velocity = velocity
		velocity = (1-effect) * old_velocity + effect * new_velocity
		
		# rotation
		var direction_normal = Vector2(-direction.y, direction.x).normalized()
		var projected = direction_normal.dot(global_position - center)
		rotation_speed += abs(old_velocity.dot(velocity)) * 0.000001 * projected

func attrack_center_of_mass(center: Vector2, center_velocity: Vector2, delta: float):

	var direction = center - global_position

	if is_connected_to_swarm:
		
		if direction.length() > outer_flock_radius:
			is_connected_to_swarm = false
			sprite.modulate.a = .5
			
		if direction.length() > inner_flock_radius:
			var acceleration =  direction.normalized() * (direction.length() - inner_flock_radius) * flock_effect_strength
			velocity = velocity + acceleration * delta

	else:
		if direction.length() < reunite_radius:
			is_connected_to_swarm = true
			sprite.modulate.a = 1

func set_flocking_parameters(
	new_inner_flock_radius: float,
	new_outer_flock_radius: float,
	new_reunite_radius: float,
	new_flock_effect_strength: float
):
	inner_flock_radius = new_inner_flock_radius
	outer_flock_radius = new_outer_flock_radius
	reunite_radius = new_reunite_radius
	flock_effect_strength = new_flock_effect_strength
	
func _on_die(sticky: bool = true, disappear: bool = false):
	if not disappear:
		var corpse = corpse_scene.instance()
		corpse.position = position
		corpse.rotation = sprite.rotation
		corpse.scale = sprite.scale
		corpse.does_stick = sticky
		corpse.velocity = velocity
		get_parent().add_child(corpse)
	queue_free()

func get_inhaled_by_person():
	queue_free()

func apply_force(force: Vector2, delta: float):
	velocity += force * delta
