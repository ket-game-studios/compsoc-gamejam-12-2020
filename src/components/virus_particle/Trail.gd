extends Particles2D

var velocity = Vector2(0, 0)

var max_velocity = 1000

var min_lifetime = 0.5
var max_lifetime = 1.0

func _process(delta):

	rotation = velocity.angle() - PI
	
	var vel = 0 
	if velocity.length() > max_velocity:
		vel = max_velocity
	else:
		vel = velocity.length()
	
	speed_scale = vel / max_velocity
	lifetime = min_lifetime + ((max_lifetime - min_lifetime) * vel/max_velocity)
