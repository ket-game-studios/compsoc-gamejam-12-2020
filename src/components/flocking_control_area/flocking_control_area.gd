extends Area2D

export var inner_flock_radius = 100.0		# radius within the flock, where particles can move freely. when they are outside this radius, particles will be pulled back in.
export var outer_flock_radius = 400.0		# radius at which particles are removed from the flock and become free.
export var reunite_radius = 300.0			# radius at which free particles are added back to the flock. Needs to be smaller than outer_flock_radius.
export var flock_effect_strength = .7		# the strength of the force pulling particles back in to the flock. (0.0 basically disables flocking)
