extends Node


enum Months {JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC, END}


var current_month = Months.JAN
var case_history = []


func _ready():
	randomize()


func advance_month():
	current_month += 1
